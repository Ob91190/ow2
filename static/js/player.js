player = []

async function getHereos() {

    const response = await axios.get('https://overfast-api.tekrop.fr/heroes')
    let heroesList = []
    heroes = response.data
    for (i = 0; i < heroes.length; i++) {
        heroesList[heroes[i].key] = heroes[i]
    }

    return heroesList
}

async function FindPlayerSummuary(playerTag) {

    playerTag = playerTag.replace("#", "-")

    const heroesList = await getHereos()


    // console.log(heroesList)

    html_role = ""
    html_player = ""
    html_header = "<th class='text-center'>Hero</th>"
    html_heroes = ""

    const roles = ["tank", "damage", "support"];
    const no_role = ["https://cdn.discordapp.com/emojis/708754921545138176.webp", "https://cdn.discordapp.com/emojis/830169249778630686.webp", "https://cdn.discordapp.com/emojis/708755148029165619.webp"]

    const playerSummuary = await axios.get('https://overfast-api.tekrop.fr/players/' + playerTag, {
            headers: {
                'Content-Type': 'application/json'
            },
        })
        // console.log(playerSummuary.data)

    player.info = playerSummuary.data.summary

    player.rank = playerSummuary.data.summary.competitive.pc

    player.stats = playerSummuary.data.stats.pc.competitive.heroes_comparisons


    //group player stats by hero

    let heroes = []

    for (stat in player.stats) {

        let stat_label = player.stats[stat].label
        let list = player.stats[stat].values


        html_header += `<th class="text-center">${stat_label}</th>`
            // console.log(list)

        for (perso in list) {

            let hero_name = list[perso].hero
            let hero_value = list[perso].value

            // console.log(hero_name)

            if (!heroes[hero_name]) {
                heroes[hero_name] = {
                    "name": hero_name,
                    "image": heroesList[hero_name].portrait,
                    "role": heroesList[hero_name].role
                }
            }

            heroes[hero_name][stat_label] = hero_value
        }

        // console.log(stat)
    }

    // console.log(player.stats)

    //

    player.heroes = heroes



    console.log(player.info)
    console.log(player.rank)
    console.log(player.stats)
    console.log(player.heroes)

    html_player = `
    
    <img id="player-header-img" src="${player.info.avatar}" class="text-center text-white align-middle mt-2 rounded-4" height="96">
    <h3 id="player-header-name" class="text-center text-white align-middle pt-2 mt-2"><span> ${player.info.username}</span></h3>
    
    `


    roles.forEach(role => {

        if (!player.rank[role]) {
            html_role += `
            <div class="col-md-4 mt-3 text-center" id="role-${role}">
                <div class="p-3 border border-secondary rounded-3 ">
                    <div class="mb-2">
                        <h3 class="text-center text-white align-middle pt-2 mt-2 mb-3 text-capitalize">${role}</h3>
                        <img style="height:100px" src="${no_role[roles.indexOf(role)]}">
                    </div>
                    <span class="text-center text-white align-middle mt-5 text-capitalize"> Noob</span>
                </div>
            </div>
        `;
        } else {

            html_role += `
          <div class="col-md-4 mt-3 text-center" id="role-${role}">
            <div class="p-3 border border-secondary rounded-3 ">
                <div class="mb-2">
                    <h3 class="text-center text-white align-middle pt-2 mt-2 mb-3 text-capitalize">${role}</h3>
                    <img style="height:100px" src="${player.rank[role].rank_icon}">
                </div>
                <span class="text-center text-white align-middle mt-5 text-capitalize"> ${player.rank[role].division} ${player.rank[role].tier}</span>
            </div>
          </div>
        `;
        }
    });


    function secondsToHours(seconds) {
        let hours = seconds / 3600
        return hours.toFixed(2)
    }


    for (hero in player.heroes) {

        html_heroes += `
        <tr>
            <td class="text-left text-capitalize"><img class="rounded-3 pr-3" src="${player.heroes[hero].image}" style="height:32px"> <span>${player.heroes[hero].name}</span></td>
            <td class="text-center">${secondsToHours(player.heroes[hero]["Time Played"])}</td>
            <td class="text-center">${player.heroes[hero]["Games Won"]}</td>
            <td class="text-center">${player.heroes[hero]["Weapon Accuracy"]}</td>
            <td class="text-center">${player.heroes[hero]["Win Percentage"]}</td>
            <td class="text-center">${player.heroes[hero]["Eliminations per Life"]}</td>
            <td class="text-center">${player.heroes[hero]["Critical Hit Accuracy"]}</td>
            <td class="text-center">${player.heroes[hero]["Multikill - Best"]}</td>
            <td class="text-center">${player.heroes[hero]["Objective Kills"]}</td>

        </tr>
        `
    }
    document.getElementById("player-role").innerHTML = html_role
    document.getElementById("player-header").innerHTML = html_player
    document.getElementById("player-stats-header").innerHTML = html_header
    document.getElementById("player-stats-list").innerHTML = html_heroes

    $(document).ready(function() {
        $('#myTable').DataTable({
                "pageLength": 100,
            }

        );
    });

    // console.log(player)


}