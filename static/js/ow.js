gameMode = ["assault", "control", "hybrid", "escort", "push"]
mapsGameMode = []
mapsInfo = []
heroesList = []
selectMaps = undefined

compoList = document.getElementById("compo-list")

getMaps()
getHereos()

function getMaps() {

    axios.get('https://overfast-api.tekrop.fr/maps')
        .then(function(response) {
            // en cas de réussite de la requête

            // console.log(response);
            maps = response.data

            for (i = 0; i < maps.length; i++) {
                if (maps[i].gamemodes.some(r => gameMode.includes(r))) {
                    // console.log(maps[i].name)
                    mapsGameMode.push(maps[i].name)
                    mapsInfo[maps[i].name] = maps[i]
                }
            }

            mapsGameMode.sort()

            if (selectMaps != undefined) {
                selectMaps.innerHTML = "<option value=''>Choisir une carte</option>"

                for (i = 0; i < mapsGameMode.length; i++) {
                    console.log(mapsGameMode[i])
                    selectMaps.innerHTML += "<option value='" + mapsGameMode[i] + "'>" + mapsGameMode[i] + "</option>"
                }
            }

        })
        .catch(function(error) {
            // en cas d’échec de la requête
            console.log(error);
        })
        .then(function() {
            // dans tous les cas
        });

}

async function getHereos() {

    const response = await axios.get('https://overfast-api.tekrop.fr/heroes')

    heroes = response.data
    for (i = 0; i < heroes.length; i++) {
        heroesList[heroes[i].name] = heroes[i]
    }

    return heroesList
}

async function CompoByMaps(selectedMap) {

    console.log(selectedMap)

    const response = await axios.get('https://ow.nsp.ovh/static/config/compoByMap.json', {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'https://ow.nsp.ovh/',
        },
    })

    console.log(response.data)
    compoType = response.data[selectedMap].types
    return compoType

}


async function heroesByCompo(selectedCompo) {

    const response = await axios.get('https://ow.nsp.ovh/static/config/compoByType.json', {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'https://ow.nsp.ovh/',
        },
    })

    heroes = response.data[selectedCompo]
    return heroes
}

async function displayCompoMap(map) {

    mapCompo = await CompoByMaps(map)
    html_compo = ""
    html_link = ""
    console.log(mapsInfo[map])

    mapType.innerHTML = `<h2><span class="badge bg-secondary text-uppercase">${mapsInfo[map].name}</span></h2><h5><span class="badge text-bg-primary text-capitalize">${mapsInfo[map].gamemodes}</span></h5>`
    mapHeaderImg.style.backgroundImage = "url(" + mapsInfo[map].screenshot + ")"


    for (i = 0; i < mapCompo.length; i++) {
        // console.log(mapCompo[i])
        heroesCompo = await heroesByCompo(mapCompo[i])
            // append componame to compoLink
        html_link += `<a href="#${mapCompo[i]}" class="badge bg-secondary text-uppercase nav-link" style="margin-right:10px;">${mapCompo[i]}</a>`

        html_compo += "<div id='" + mapCompo[i] + "' class='compo-type mt-5'><h3>" + mapCompo[i] + "</h3><hr class='border border-primary border-2 opacity-75'>"
        html_compo += "<h4>Dps</h4><hr><div class='compo-heroes d-flex'>"

        heroesCompo["dps"].sort()
        heroesCompo["tank"].sort()
        heroesCompo["support"].sort()

        for (j = 0; j < heroesCompo["dps"].length; j++) {
            html_compo += "<div style='margin-right:15px;' class='compo-hero rounded-3 bg-secondary'><img style='height:128px' src='" + heroesList[heroesCompo["dps"][j]].portrait + "' alt='" + heroesCompo["dps"][j] + "'><p class='text-center mb-0 text-white'>" + heroesCompo["dps"][j] + "</p></div>"
        }

        html_compo += "</div><h4 class='mt-4'>Tank</h4><hr><div class='compo-heroes d-flex'>"

        for (j = 0; j < heroesCompo["tank"].length; j++) {
            html_compo += "<div style='margin-right:15px;' class='compo-hero rounded-3 bg-secondary'><img style='height:128px' src='" + heroesList[heroesCompo["tank"][j]].portrait + "' alt='" + heroesCompo["tank"][j] + "'><p class='text-center mb-0 text-white' >" + heroesCompo["tank"][j] + "</p></div>"
        }

        html_compo += "</div><h4 class='mt-4'>Support</h4><hr><div class='compo-heroes d-flex'>"

        for (j = 0; j < heroesCompo["support"].length; j++) {
            html_compo += "<div style='margin-right:15px;' class='compo-hero rounded-3 bg-secondary'><img style='height:128px' src='" + heroesList[heroesCompo["support"][j]].portrait + "' alt='" + heroesCompo["support"][j] + "'><p class='text-center mb-0 text-white'>" + heroesCompo["support"][j] + "</p></div>"

        }

        html_compo += "</div></div>"

        compoList.innerHTML = html_compo

    }

    compoLink.innerHTML = html_link

}

async function displayallCompo() {

    allcompo = ["Dive", "Rush", "Poke"]
    html_compo = ""
    console.log(allcompo)

    heroesList = await getHereos()
    console.log(heroesList)

    for (i = 0; i < allcompo.length; i++) {
        console.log(allcompo[i])
        heroesCompo = await heroesByCompo(allcompo[i])
        console.log(heroesCompo)
        html_compo += "<div class='compo-type'><h3>" + allcompo[i] + "</h3>"
        html_compo += "<h4>Dps</h4><div class='compo-heroes d-flex'>"

        heroesCompo["dps"].sort()
        heroesCompo["tank"].sort()
        heroesCompo["support"].sort()



        for (j = 0; j < heroesCompo["dps"].length; j++) {

            console.log(heroesCompo["dps"][j])
            console.log(heroesList[heroesCompo["dps"][j]])
            html_compo += "<div class='compo-hero'><img style='height:128px' src='" + heroesList[heroesCompo["dps"][j]].portrait + "' alt='" + heroesCompo["dps"][j] + "'><p>" + heroesCompo["dps"][j] + "</p></div>"
        }

        html_compo += "</div><h4>Tank</h4><div class='compo-heroes d-flex'>"

        for (j = 0; j < heroesCompo["tank"].length; j++) {

            // console.log(heroesList[heroesCompo["tank"][j]])
            html_compo += "<div class='compo-hero'><img style='height:128px' src='" + heroesList[heroesCompo["tank"][j]].portrait + "' alt='" + heroesCompo["tank"][j] + "'><p>" + heroesCompo["tank"][j] + "</p></div>"

        }

        html_compo += "</div><h4>Support</h4><div class='compo-heroes d-flex'>"

        for (j = 0; j < heroesCompo["support"].length; j++) {

            // console.log(heroesList[heroesCompo["support"][j]])
            html_compo += "<div class='compo-hero'><img style='height:128px' src='" + heroesList[heroesCompo["support"][j]].portrait + "' alt='" + heroesCompo["support"][j] + "'><p>" + heroesCompo["support"][j] + "</p></div>"

        }

        html_compo += "</div></div>"

        compoList.innerHTML = html_compo

    }

}